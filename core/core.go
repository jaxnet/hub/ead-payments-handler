package core

import (
	"ead-payments-handler/core/database"
	"ead-payments-handler/core/handlers/processing"
	"ead-payments-handler/core/interfaces/http"
	"ead-payments-handler/core/keychain"
	"ead-payments-handler/core/logger"
	"gitlab.com/jaxnet/hub/ead/core/ec"
)

func NewCore() (err error) {
	logger.Log.Info().Msg("Initializing database")
	{
		err = database.InitConnectionsPool()
		ec.InterruptOnError(err)
	}
	logger.Log.Info().Msg("Database initialized successfully")

	logger.Log.Info().Msg("Initializing database schema")
	{
		err = database.EnsureSchema()
		ec.InterruptOnError(err)
	}
	logger.Log.Info().Msg("Database schema initialized successfully")

	err = keychain.InitKeyChain()
	ec.InterruptOnError(err)

	err = processing.NewShardsHandler()
	ec.InterruptOnError(err)

	err = processing.CheckAndRetrySendProcessingTransactions()
	ec.InterruptOnError(err)

	processing.RunShardsProcessing()
	go processing.RunProcessing()

	logger.Log.Info().Msg("Core initialization finished")
	return
}

func RunUntilError() (err error) {
	err = http.InitHHTPInterface()
	if err != nil {
		return
	}

	select {
	case err = <-http.RunHTTPInterface():
	}

	return
}
