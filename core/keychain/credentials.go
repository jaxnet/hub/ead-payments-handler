package keychain

import (
	"io/ioutil"
)

const filePath = "pkey.key"

func LoadCredentials() (pKeyHex string, err error) {

	pKeyHexBinary, err := ioutil.ReadFile(filePath)
	if err != nil {
		return
	}

	pKeyHex = string(pKeyHexBinary)
	return
}
