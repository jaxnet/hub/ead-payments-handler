package keychain

import (
	"ead-payments-handler/core/settings"
	"encoding/hex"
	"gitlab.com/jaxnet/core/shard.core/btcutil"
	"gitlab.com/jaxnet/core/shard.core/cmd/tx-gatling/txutils"
	"gitlab.com/jaxnet/core/shard.core/types/chainhash"
	"gitlab.com/jaxnet/core/shard.core/types/wire"
)

type PermanentKeychain struct {
	pKeyHex string
	keyData *txutils.KeyData
}

var (
	KeyChain *PermanentKeychain
)

func InitKeyChain() (err error) {
	tmpPKeyHex, err := LoadCredentials()
	if err != nil {
		return
	}
	KeyChain = &PermanentKeychain{
		pKeyHex: tmpPKeyHex,
	}
	KeyChain.keyData, err = txutils.NewKeyData(tmpPKeyHex, settings.Conf.Network)
	if err != nil {
		return
	}
	return
}

func (k *PermanentKeychain) NewTxMan(shardConf *settings.ShardConfig) (txMan *txutils.TxMan, err error) {
	gatlingRPCParams := txutils.ManagerCfg{
		Net:     settings.Conf.Blockchain.NetworkName,
		ShardID: uint32(shardConf.ID),
		RPC: txutils.NodeRPC{
			Host: shardConf.RPC.Net.Interface(),
			User: shardConf.RPC.Credentials.User,
			Pass: shardConf.RPC.Credentials.Pass,
		},
		PrivateKey: k.pKeyHex,
	}

	txMan, err = txutils.NewTxMan(gatlingRPCParams)
	if err != nil {
		return
	}

	txMan = txMan.ForShard(uint32(shardConf.ID))
	return
}

func (k *PermanentKeychain) AddressPubKeyHash() (address *btcutil.AddressPubKeyHash) {
	address = k.keyData.AddressPubKey.AddressPubKeyHash()
	return
}

func (k *PermanentKeychain) CreateTransaction(txBuilder txutils.TxBuilder, feeFunc txutils.FeeProviderFunc) (
	transaction *wire.MsgTx, err error) {

	transaction, err = txBuilder.IntoTx(feeFunc, k.keyData)
	return
}

func (k *PermanentKeychain) SignDataForIndexer(data string) (pubKey, sig string, err error) {
	hash := chainhash.DoubleHashB([]byte(data))
	signature, err := k.keyData.PrivateKey.Sign(hash)
	if err != nil {
		return
	}

	pubKey = hex.EncodeToString(k.keyData.PrivateKey.PubKey().SerializeUncompressed())
	sig = hex.EncodeToString(signature.Serialize())
	return
}
