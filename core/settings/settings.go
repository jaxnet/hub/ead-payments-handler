package settings

import (
	"fmt"
	"gitlab.com/jaxnet/core/shard.core/types/chaincfg"
	"gitlab.com/jaxnet/hub/ead/core/blockchain/shards"
	"gopkg.in/yaml.v2"
	"io/ioutil"
)

var (
	Conf *Settings
)

type Interfaces struct {
	Http           NetworkConfig `yaml:"http"`
	ExchangeAgents NetworkConfig `yaml:"exchange-agents"`
	Clients        NetworkConfig `yaml:"clients"`
}

type ShardConfig struct {
	ID                              shards.ID `yaml:"id"`
	RPC                             RPCConfig `yaml:"rpc"`
	NonSpentUtxosIndexerHostAndPort string    `yaml:"non-spent-utxos-indexer-host-and-port"`
}

type BlockchainConfig struct {
	NetworkName string `yaml:"network-name"`
	//
	//	// Shards are stored as an array in conf.yaml,
	//	// It is much more suitable to operate them as a map of shards configs,
	//	// but in this case, config goes to be a bit ugly.
	//	// To handle it, slice structure is going to be copied into the map on settings initialisation.
	Shards []ShardConfig `yaml:"shards"`
	//
	//	// Due to the bug in current yaml-parser implementation,
	//	// there is no way to parse field from YAML-file to the private field, or field with different name.
	//	// So, there were a choice: to rename "shards" section in config file, or to use different name for map.
	ShardsMap map[shards.ID]*ShardConfig `yaml:"-"`
}

func (c *BlockchainConfig) normalize() {
	// Moving Shards configs from slice into the map.
	// See `Shards` field docs for the details.
	c.ShardsMap = make(map[shards.ID]*ShardConfig)
	for i, shardConf := range c.Shards {
		c.ShardsMap[shardConf.ID] = &c.Shards[i]
	}
}

type DatabaseConfig RPCConfig

func (c DatabaseConfig) PSQLConnectionCredentials() string {
	return fmt.Sprint("postgres://", c.Credentials.User, ":", c.Credentials.Pass, "@", c.Net.Address, ":", c.Net.Port, "/ead_payments_handler")
}

type PaymentsSettings struct {
	TransactionsPerBlock uint32 `yaml:"transactions-per-block"`
	RoundDelaySeconds    uint16 `yaml:"round-delay-seconds"`
}

type ShardsSettings struct {
	ConfirmationsRequired         int64  `yaml:"confirmations-required"`
	AvgBlockGenerationTimeSeconds uint16 `yaml:"avg-block-generation-time-seconds"`
}

type BlockChainSettings struct {
	Shards ShardsSettings `yaml:"shards"`
	Beacon ShardsSettings `yaml:"beacon"`
}

type HandlerSettings struct {
	Payments               PaymentsSettings   `yaml:"payments"`
	Blockchain             BlockChainSettings `yaml:"blockchain"`
	MinRequiredFeesUTXOCnt uint32             `yaml:"min-required-fees-utxo-cnt"`
	RebalancingUTXOsCnt    uint32             `yaml:"rebalancing-utxos-cnt"`
}

type Settings struct {
	Debug      bool             `yaml:"debug"`
	Interfaces Interfaces       `yaml:"interfaces"`
	Blockchain BlockchainConfig `yaml:"blockchain"`
	Network    *chaincfg.Params
	Database   DatabaseConfig  `yaml:"database"`
	Handler    HandlerSettings `yaml:"handler"`
}

func LoadSettings() (err error) {
	Conf = &Settings{}

	data, err := ioutil.ReadFile("conf.yaml")
	if err != nil {
		return
	}

	err = yaml.Unmarshal(data, Conf)
	if err != nil {
		return
	}

	if Conf.Blockchain.NetworkName == "fastnet" {
		Conf.Network = &chaincfg.FTestNetParams
	}

	Conf.normalize()

	//err = Conf.validate()
	//if err != nil {
	//	return
	//}

	// todo: temporary implementation here
	//Conf.assignNetworkConfAccordingToSpecifiedNetwork()
	return
}

// normalize calls the same methods of internal structures,
// to processes loaded parameters and change their representation if it is needed.
func (s *Settings) normalize() {
	s.Blockchain.normalize()
}
