package settings

import "fmt"

type CredentialsConfig struct {
	User string `yaml:"user"`
	Pass string `yaml:"pass"`
}

type NetworkConfig struct {
	Address string `yaml:"address"`
	Port    uint16 `yaml:"port"`
}

func (c NetworkConfig) Interface() string {
	return fmt.Sprint(c.Address, ":", c.Port)
}

type RPCConfig struct {
	Net         NetworkConfig
	Credentials CredentialsConfig
}
