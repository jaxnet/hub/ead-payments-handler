begin transaction;

create table if not exists payments (
    id bigserial primary key,
    constraint id_gte_zero check (id >= 0),

    uuid uuid not null,
    constraint unique_uuid unique (uuid),

    shard_id bigint,
    constraint shard_id_gte_zero check (shard_id >= 0), /* beacon chain is supported */

    amount bigint not null,
    constraint amount_gt_zero check (amount > 0),

    address text not null,

    max_blocks_height bigint,
    constraint max_blocks_height_gte_zero check (max_blocks_height >= 0),

    state smallint not null default 0,
    constraint state_ge_zero check (state >= 0),

    txid bigint,
    constraint txid_gte_zero check (txid >= 0),

    tx_out_num bigint,
    constraint tx_out_num_gte_zero check (tx_out_num >= 0),

    created timestamp with time zone default now()
);

create table if not exists transactions (
    id bigserial primary key,
    constraint id_gte_zero check (id >= 0),

    tx_data bytea not null,

    shard_id bigint not null,
    constraint shard_id_gte_zero
    check (shard_id >= 0),

    state smallint not null default 0,
    constraint state_ge_zero check (state >= 0),

    is_contains_rebalancing_payments bool,

    error_description text null default null,
    created timestamp with time zone default now());

end;