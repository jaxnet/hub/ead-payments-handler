package http

import (
	"ead-payments-handler/core/settings"
	"fmt"
	"github.com/google/uuid"
	"gitlab.com/jaxnet/core/shard.core/btcutil"
	"gitlab.com/jaxnet/hub/ead/core/blockchain/shards"
	"gitlab.com/jaxnet/hub/ead/core/ec"
	"strconv"
)

// todo: Tests needed
func validateShardID(shardID string) (id shards.ID, err error) {
	var (
		invalidShardID = fmt.Errorf(
			"shard id must be a positive number greater or equal to 1: (%w)", ec.ErrInvalidShardID)

		noShardFound = fmt.Errorf(
			"no shard with specified ID found, check settings: (%w)", ec.ErrInvalidShardID)
	)

	i, err := strconv.ParseInt(shardID, 10, 32)
	if err != nil || i < 1 {
		err = invalidShardID
		return
	}

	id = shards.ID(i)
	_, shardIsPresent := settings.Conf.Blockchain.ShardsMap[id]
	if !shardIsPresent {
		err = noShardFound
		return
	}

	return
}

func validateUUID(stringUUID string) (validatedUUID uuid.UUID, err error) {
	validatedUUID, err = uuid.Parse(stringUUID)
	return
}

func validateBlockNumber(blockNumber string) (validatedBlockNumber uint64, err error) {
	var (
		invalidBlockNumber = fmt.Errorf(
			"block number must be a positive number greater or equal to 1: (%w)", ec.ErrInvalidShardID)
	)

	i, err := strconv.ParseInt(blockNumber, 10, 32)
	if err != nil || i < 1 {
		err = invalidBlockNumber
		return
	}

	validatedBlockNumber = uint64(i)

	return
}

func validateAmount(amount string) (normalized btcutil.Amount, err error) {
	var (
		invalidAmount = fmt.Errorf(
			"amount must be a positive number: (%w)", ec.ErrInvalidReserve)
	)
	i, err := strconv.ParseInt(amount, 10, 64)
	if err != nil || i < 1 {
		err = invalidAmount
		return
	}
	normalized = btcutil.Amount(i)
	return
}

func validateAddress(address string) (validatedAddress string, err error) {

	validatedAddress = address
	return
}
