package http

import (
	"ead-payments-handler/core/handlers/payments"
	"ead-payments-handler/core/handlers/transactions"
	"ead-payments-handler/core/logger"
	"ead-payments-handler/core/settings"
	"encoding/json"
	"fmt"
	hubtransactions "gitlab.com/jaxnet/hub/ead/core/handlers/transactions"
	"net/http"
	"strings"
)

func invalidParameterPlaceholder(parameter string) (placeholder string) {
	return fmt.Sprint("parameter `", parameter, "` is invalid: %s")
}

var (
	argShardID         = "shard-id"
	argUUID            = "uuid"
	argAmount          = "amount"
	argAddress         = "address"
	argMaxBlocksHeight = "max-blocks-height"

	invalidBodyMsg            = "invalid/corrupted request body"
	invalidShardIDMsg         = invalidParameterPlaceholder(argShardID)
	invalidUUIDMsg            = invalidParameterPlaceholder(argUUID)
	invalidAmountMsg          = invalidParameterPlaceholder(argAmount)
	invalidAddressMsg         = invalidParameterPlaceholder(argAddress)
	invalidMaxBlocksHeightMsg = invalidParameterPlaceholder(argMaxBlocksHeight)
)

func InitHHTPInterface() (err error) {

	http.HandleFunc("/api/v1/payments/", func(w http.ResponseWriter, r *http.Request) {
		if r.Method == "POST" {
			handleAddPayment(w, r)

		} else {
			w.WriteHeader(http.StatusBadRequest)
			return
		}
	})
	http.HandleFunc("/api/v1/payments/state/", func(w http.ResponseWriter, r *http.Request) {
		if r.Method == "POST" {
			handleGetPaymentState(w, r)

		} else {
			w.WriteHeader(http.StatusBadRequest)
			return
		}
	})
	return
}

func RunHTTPInterface() (errors <-chan error) {
	errorsFlow := make(chan error)

	logger.Log.Info().Str(
		"interface", settings.Conf.Interfaces.Http.Interface()).Msg(
		"Initializing admin HTTP interface")

	go func() {
		errorsFlow <- http.ListenAndServe(settings.Conf.Interfaces.Http.Interface(), nil)
	}()

	logger.Log.Info().Msg("HTTP interface initialization is done")
	return errorsFlow
}

func handleAddPayment(w http.ResponseWriter, r *http.Request) {

	logger.Log.Debug().Msg("Add payment request")
	err := r.ParseForm()
	if err != nil {
		logger.Log.Err(err).Msg("invalidBodyMsg")
		reportBadRequest(w, invalidBodyMsg)
		return
	}

	shardID, err := validateShardID(r.Form.Get(argShardID))
	if err != nil {
		logger.Log.Err(err).Msg("Invalid shardID request param")
		msg := fmt.Sprintf(invalidShardIDMsg, err)
		reportBadRequest(w, msg)
		return
	}
	logger.Log.Debug().Uint64("shardID", uint64(shardID)).Msg("Request param")

	uuid, err := validateUUID(r.Form.Get(argUUID))
	if err != nil {
		logger.Log.Err(err).Msg("Invalid uuid request param")
		msg := fmt.Sprintf(invalidUUIDMsg, err)
		reportBadRequest(w, msg)
		return
	}
	logger.Log.Debug().Str("uuid", uuid.String()).Msg("Request param")

	amount, err := validateAmount(r.Form.Get(argAmount))
	if err != nil {
		logger.Log.Err(err).Msg("Invalid amount request param")
		msg := fmt.Sprintf(invalidAmountMsg, err)
		reportBadRequest(w, msg)
		return
	}
	logger.Log.Debug().Int64("amount", int64(amount)).Msg("Request param")

	address, err := validateAddress(r.Form.Get(argAddress))
	if err != nil {
		logger.Log.Err(err).Msg("Invalid address request param")
		msg := fmt.Sprintf(invalidAddressMsg, err)
		reportBadRequest(w, msg)
		return
	}
	logger.Log.Debug().Str("address", address).Msg("Request param")

	maxBlockHeight, err := validateBlockNumber(r.Form.Get(argMaxBlocksHeight))
	if err != nil {
		logger.Log.Err(err).Msg("Invalid max-blocks-height request param")
		msg := fmt.Sprintf(invalidMaxBlocksHeightMsg, err)
		reportBadRequest(w, msg)
		return
	}
	logger.Log.Debug().Uint64("max-blocks-height", maxBlockHeight).Msg("Request param")

	paymentExists, err := payments.CheckIfPaymentExists(uuid)
	if err != nil {
		logger.Log.Err(err).Msg("Can't check payment existing into database")
		reportError(w, err)
		return
	}

	if paymentExists {
		logger.Log.Info().Uint32("shard-id", uint32(shardID)).Str("uuid", uuid.String()).
			Int64("amount", int64(amount)).Str("address", address).
			Uint64("max-block-height", maxBlockHeight).
			Msg("Payment already exists in database. Payment request was processed")
		w.WriteHeader(http.StatusOK)
		return
	}

	err = payments.WritePaymentToDatabase(payments.Payment{
		ShardID:         shardID,
		UUID:            uuid,
		Amount:          amount,
		Address:         address,
		MaxBlocksHeight: maxBlockHeight})
	if err != nil {
		logger.Log.Err(err).Msg("Can't write payment into database")
		reportError(w, err)
		return
	}
	logger.Log.Info().Uint32("shard-id", uint32(shardID)).Str("uuid", uuid.String()).
		Int64("amount", int64(amount)).Str("address", address).
		Uint64("max-block-height", maxBlockHeight).Msg("Payment request was processed")
	w.WriteHeader(http.StatusOK)
}

func handleGetPaymentState(w http.ResponseWriter, r *http.Request) {

	logger.Log.Debug().Msg("Get payment status request")
	err := r.ParseForm()
	if err != nil {
		reportBadRequest(w, invalidBodyMsg)
		return
	}

	uuid, err := validateUUID(r.Form.Get(argUUID))
	if err != nil {
		logger.Log.Err(err).Msg("Invalid uuid request param")
		msg := fmt.Sprintf(invalidUUIDMsg, err)
		reportBadRequest(w, msg)
		return
	}
	logger.Log.Debug().Str("uuid", uuid.String()).Msg("Request get payment status")

	payment, err := payments.FetchPaymentByUUID(uuid)
	if err != nil {
		logger.Log.Err(err).Msg("Can't get payment from DB")
		reportError(w, err)
		return
	}
	logger.Log.Debug().Uint8("state", payment.State).Msg("Result from DB")

	response := hubtransactions.PaymentStateResponse{State: payment.State}

	if payment.State == payments.Confirmed {
		transaction, err := transactions.FetchTransactionsWithId(payment.TransactionID)
		if err != nil {
			logger.Log.Err(err).Msg("Can't get transaction from DB")
			reportError(w, err)
			return
		}
		txHash := transaction.TxData.TxHash()
		response.TxHash = txHash[:]
		response.TxOutNum = payment.TxOutNum
		logger.Log.Info().Str("uuid", uuid.String()).Uint8("state", payment.State).
			Str("tx-hash", txHash.String()).Uint64("tx-out-num", payment.TxOutNum).
			Msg("Get payment state request was processed")
	}

	w.WriteHeader(http.StatusOK)
	js, err := json.Marshal(response)
	if err != nil {
		logger.Log.Err(err).Msg("Can't marshall data")
		reportError(w, err)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	_, err = w.Write(js)
	if err != nil {
		logger.Log.Err(err).Msg("Can't write response data")
	}
}

func reportError(w http.ResponseWriter, err error) {
	message := strings.Replace(err.Error(), `"`, `\"`, -1)
	description := fmt.Sprintf(`{"error": "%s"}`, message)
	_, _ = w.Write([]byte(description))
	w.WriteHeader(http.StatusInternalServerError)
}

func reportBadRequest(w http.ResponseWriter, message string) {
	description := fmt.Sprintf(`{"error": "%s"}`, message)
	_, _ = w.Write([]byte(description))
	w.WriteHeader(http.StatusBadRequest)
}

func reportBadRequestFromError(w http.ResponseWriter, err error) {
	msg := fmt.Sprintf(err.Error())
	reportBadRequest(w, msg)
}
