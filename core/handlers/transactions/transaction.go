package transactions

import (
	"bytes"
	"ead-payments-handler/core/handlers/payments"
	"gitlab.com/jaxnet/core/shard.core/types/wire"
	"gitlab.com/jaxnet/hub/ead/core/blockchain/shards"
	"time"
)

const (
	Initialised = 0
	Processing  = 1
	Pending     = 2
	Confirmed   = 3
	Expired     = 4
	Error       = 255
)

type Transaction struct {
	ID            uint64
	State         uint8
	ShardID       shards.ID
	TxData        *wire.MsgTx
	Created       *time.Time // Could be nil.
	IsRebalancing bool
}

func TransactionFromDBData(id int64, shardID shards.ID, txDataBytes []byte,
	state int16, created *time.Time, isRebalansing bool) (tx *Transaction, err error) {

	tx = &Transaction{}
	tx.ID, err = payments.ParseAndValidateID(id)
	if err != nil {
		return
	}

	tx.ShardID = shardID

	tx.State, err = payments.ParseAndValidateState(state)
	if err != nil {
		return
	}

	tx.Created, err = payments.ParseAndValidateTime(created, false)
	if err != nil {
		return
	}

	var txData wire.MsgTx
	bytes.NewReader(txDataBytes)
	err = txData.Deserialize(bytes.NewReader(txDataBytes))
	if err != nil {
		return
	}
	tx.TxData = &txData

	tx.IsRebalancing = isRebalansing
	return
}
