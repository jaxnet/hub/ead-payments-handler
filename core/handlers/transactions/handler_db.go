package transactions

import (
	"bytes"
	"context"
	"ead-payments-handler/core/database"
	"encoding/hex"
	"errors"
	"fmt"
	"gitlab.com/jaxnet/hub/ead/core/blockchain/shards"
	"strconv"
	"time"
)

func WriteTransactionToDatabase(transaction *Transaction) (err error) {
	tx, err := database.DB.Begin(context.Background())
	if err != nil {
		return
	}

	// Rollback is safe to call even if the tx is already closed, so if
	// the tx commits successfully, this is a no-op
	defer tx.Rollback(context.Background())

	buf := bytes.NewBuffer(nil)
	err = transaction.TxData.Serialize(buf)
	if err != nil {
		return
	}
	hexRepresentation := hex.EncodeToString(buf.Bytes())
	txDataHexRepresentation := fmt.Sprint("decode('", hexRepresentation, "', 'hex')")

	shardIDStr := fmt.Sprint(int64(transaction.ShardID))
	stateStr := strconv.Itoa(int(transaction.State))
	isRebalansingStr := strconv.FormatBool(transaction.IsRebalancing)
	query := fmt.Sprint("INSERT INTO transactions (shard_id, tx_data, state, is_contains_rebalancing_payments) VALUES (",
		shardIDStr, ", ", txDataHexRepresentation, ", ", stateStr, ", ", isRebalansingStr, ") RETURNING id")

	row := tx.QueryRow(context.Background(),
		query)

	var recordID uint64
	err = row.Scan(&recordID)
	if err != nil {
		err = fmt.Errorf("can't write new payments to the database: %w", err)
		return
	}

	err = tx.Commit(context.Background())
	transaction.ID = recordID
	return
}

func FetchTransactionsWithState(state uint8) (transactionsList map[uint64]*Transaction, err error) {
	query := fmt.Sprintf(
		"SELECT "+
			"id, shard_id, tx_data, state, created, is_contains_rebalancing_payments "+
			"FROM transactions "+
			"WHERE state = %v ", state)

	rows, err := database.DB.Query(context.Background(), query)
	if err != nil {
		return
	}
	defer rows.Close()

	transactionsList = make(map[uint64]*Transaction)
	for rows.Next() {
		values, err := rows.Values()
		if err != nil {
			return nil, err
		}

		var txDataBytes []byte
		switch v := values[2].(type) {
		case []byte:
			txDataBytes = v
		}

		var created *time.Time = nil
		switch v := values[4].(type) {
		case time.Time:
			created = &v
		}

		tx, err := TransactionFromDBData(
			values[0].(int64),
			shards.ID(values[1].(int64)),
			txDataBytes,
			values[3].(int16),
			created, values[5].(bool))

		if err != nil {
			return nil, err
		}

		transactionsList[tx.ID] = tx
	}
	return
}

func FetchTransactionsWithId(id uint64) (transaction *Transaction, err error) {
	query := fmt.Sprintf(
		"SELECT "+
			"id, shard_id, tx_data, state, created, is_contains_rebalancing_payments "+
			"FROM transactions "+
			"WHERE id = %v ", id)

	rows, err := database.DB.Query(context.Background(), query)
	if err != nil {
		return
	}
	defer rows.Close()

	if rows.Next() {
		values, err := rows.Values()
		if err != nil {
			return nil, err
		}

		var txDataBytes []byte
		switch v := values[2].(type) {
		case []byte:
			txDataBytes = v
		}

		var created *time.Time = nil
		switch v := values[4].(type) {
		case time.Time:
			created = &v
		}

		transaction, err = TransactionFromDBData(
			values[0].(int64),
			shards.ID(values[1].(int64)),
			txDataBytes,
			values[3].(int16),
			created, values[5].(bool))

		if err != nil {
			return nil, err
		}
	} else {
		err = errors.New("There are no requested payment")
	}
	return
}

func UpdateTransactionState(transaction *Transaction, newState uint8) (err error) {
	tx, err := database.DB.Begin(context.Background())
	if err != nil {
		return
	}

	// Rollback is safe to call even if the tx is already closed, so if
	// the tx commits successfully, this is a no-op
	defer tx.Rollback(context.Background())

	_, err = tx.Exec(context.Background(),
		"UPDATE transactions SET state = $1 where id = $2",
		newState,
		transaction.ID)
	if err != nil {
		return
	}

	err = tx.Commit(context.Background())
	transaction.State = newState
	return
}

func IsRebalansingTransactionsWithStatePresent(shardID shards.ID, state uint8) (isPresent bool, err error) {
	query := fmt.Sprintf(
		"SELECT 1 FROM transactions WHERE shard_id = %d AND state = %d  AND is_contains_rebalancing_payments = true",
		shardID, state)
	rows, err := database.DB.Query(context.Background(), query)
	if err != nil {
		return
	}
	defer rows.Close()

	isPresent = rows.Next()
	return
}
