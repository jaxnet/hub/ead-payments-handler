package processing

import (
	"ead-payments-handler/core/handlers/payments"
	"ead-payments-handler/core/handlers/transactions"
	"ead-payments-handler/core/logger"
	"ead-payments-handler/core/settings"
	"gitlab.com/jaxnet/core/shard.core/cmd/tx-gatling/txutils"
	"time"
)

func RunProcessing() {
	time.Sleep(time.Second * 5)

	for {
		logger.Log.Debug().Msg("TP: next iteration")
		pendingTransactions, err := transactions.FetchTransactionsWithState(transactions.Pending)
		if err != nil {
			logger.Log.Err(err).Msg("Can't get pending transactions from DB")
			postponeTransactionProcessing()
			continue
		}

		if len(pendingTransactions) == 0 {
			logger.Log.Debug().Msg("TP: There are no suitable transactions")
			time.Sleep(time.Second * time.Duration(settings.Conf.Handler.Payments.RoundDelaySeconds))
			postponeTransactionProcessing()
			continue
		}

		for _, pendingTransaction := range pendingTransactions {
			logger.Log.Debug().Uint64("id", pendingTransaction.ID).Str(
				"hash", pendingTransaction.TxData.TxHash().String()).Msg(
				"TP: process transaction")
			txMan, err := GetTxManForShard(pendingTransaction.ShardID)
			if err != nil {
				logger.Log.Err(err).Uint64("id", pendingTransaction.ID).Str(
					"hash", pendingTransaction.TxData.TxHash().String()).Msg(
					"TP: Can't get txMan for pending transaction")
				postponeTransactionProcessing()
				continue
			}
			txHash := pendingTransaction.TxData.TxHash()
			out, err := txMan.ForShard(uint32(pendingTransaction.ShardID)).RPC().GetTxOut(
				&txHash, 0, true, false)
			if err != nil {
				logger.Log.Err(err).Uint64("id", pendingTransaction.ID).Str(
					"hash", pendingTransaction.TxData.TxHash().String()).Msg(
					"TP: Can't check transaction in blockchain")
				postponeTransactionProcessing()
				continue
			}

			if out == nil {
				logger.Log.Info().Uint64("id", pendingTransaction.ID).Str(
					"hash", pendingTransaction.TxData.TxHash().String()).Msg(
					"No lock funds TX found in chain. Waiting...")
				postponeTransactionProcessing()
				continue
			}

			var countConfirmationsRequired int64
			if pendingTransaction.ShardID == 0 {
				countConfirmationsRequired = settings.Conf.Handler.Blockchain.Beacon.ConfirmationsRequired
			} else {
				countConfirmationsRequired = settings.Conf.Handler.Blockchain.Shards.ConfirmationsRequired
			}

			if out.Confirmations <= countConfirmationsRequired {
				logger.Log.Info().Int64(
					"confirmations", out.Confirmations).Msg(
					"TP: Lock funds TX found, but not enough confirmations")
				postponeTransactionProcessing()
				continue
			}

			logger.Log.Info().Msg("TP: Lock TX found and mined with enough confirmations")
			err = transactions.UpdateTransactionState(pendingTransaction, transactions.Confirmed)
			if err != nil {
				logger.Log.Err(err).Uint64("id", pendingTransaction.ID).Str(
					"hash", pendingTransaction.TxData.TxHash().String()).Msg(
					"TP: Can't update transaction state in DB")
				postponeTransactionProcessing()
				continue
			}

			err = payments.UpdatePaymentsStateByTransactionID(
				pendingTransaction.ID, payments.Confirmed)
			if err != nil {
				logger.Log.Err(err).Uint64("id", pendingTransaction.ID).Str(
					"hash", pendingTransaction.TxData.TxHash().String()).Msg(
					"Can't update payments state in DB")
				postponeTransactionProcessing()
				continue
			}
		}
		postponeTransactionProcessing()
	}
}

func CheckAndRetrySendProcessingTransactions() (err error) {
	logger.Log.Debug().Msg("TP: Check and retry send processing transactions")
	checkingTransactions, err := transactions.FetchTransactionsWithState(transactions.Initialised)
	if err != nil {
		return
	}
	if len(checkingTransactions) == 0 {
		logger.Log.Info().Msg("TP: There are no processing transactions")
		return
	}

	for _, checkingTransaction := range checkingTransactions {
		logger.Log.Debug().Str("Tx hash", checkingTransaction.TxData.TxHash().String()).Msg(
			"TP: Processing transaction")
		txMan, err1 := GetTxManForShard(checkingTransaction.ShardID)
		if err1 != nil {
			err = err1
			return
		}
		txHash := checkingTransaction.TxData.TxHash()
		_, err1 = txMan.RPC().GetRawTransaction(&txHash, false)
		if err1 == nil {
			logger.Log.Info().Msg("TP: Transaction already in blockchain")
			err1 = transactions.UpdateTransactionState(checkingTransaction, transactions.Pending)
			if err1 != nil {
				return err1
			}
			continue
		}

		ok, err1 := checkIfAllPaymentsAreNotExpired(checkingTransaction, txMan)
		if err1 != nil {
			logger.Log.Info().Err(err1).Msg("")
			return err1
		}
		if !ok {
			logger.Log.Info().Str("txHash", txHash.String()).Msg("TP : Transaction is outdated")
			err1 = transactions.UpdateTransactionState(checkingTransaction, transactions.Expired)
			if err1 != nil {
				return err1
			}
			continue
		}
		_, err1 = txMan.RPC().SendRawTransaction(checkingTransaction.TxData)
		if err1 != nil {
			logger.Log.Err(err1).Str("tx hash", checkingTransaction.TxData.TxHash().String()).Msg(
				"TP: Can't send transaction to blockchain")
			// todo : update transaction state
			continue
		}
		logger.Log.Info().Str("tx hash", checkingTransaction.TxData.TxHash().String()).Msg(
			"TP : Transaction sent to blockchain")

		err1 = transactions.UpdateTransactionState(checkingTransaction, transactions.Pending)
		if err1 != nil {
			return err1
		}
		logger.Log.Info().Str("txHash", txHash.String()).Msg("TP : Transaction in blockchain and will be checked")
	}
	return
}

func checkIfAllPaymentsAreNotExpired(transaction *transactions.Transaction, txMan *txutils.TxMan) (ok bool, err error) {
	checkedPayments, err := payments.FetchPaymentsForTransaction(transaction.ID)
	if err != nil {
		return
	}

	_, currentBlockNumber, err := txMan.ForShard(uint32(transaction.ShardID)).RPC().GetBestBlock()
	if err != nil {
		logger.Log.Err(err).Uint64("shardID", uint64(transaction.ShardID)).Msg(
			"ShP: Can't get block number from blockchain")
		return
	}

	ok = true
	for _, checkedPayment := range checkedPayments {
		// todo : also check payment status. it should be PENDING
		if checkedPayment.MaxBlocksHeight < uint64(currentBlockNumber) {
			ok = false
			break
		}
	}
	if ok {
		return
	}

	// set all payments into INIT state for further processing
	for _, checkedPayment := range checkedPayments {
		err = payments.UpdatePaymentStateAndTxID(checkedPayment, payments.Initialised, 0)
		if err != nil {
			return
		}
	}
	return
}

func postponeTransactionProcessing() {
	time.Sleep(time.Second * time.Duration(settings.Conf.Handler.Payments.RoundDelaySeconds))
}
