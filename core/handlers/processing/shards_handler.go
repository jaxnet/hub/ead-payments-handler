package processing

import (
	"ead-payments-handler/core/keychain"
	"ead-payments-handler/core/logger"
	"ead-payments-handler/core/settings"
	"errors"
	"gitlab.com/jaxnet/core/shard.core/cmd/tx-gatling/txutils"
	"gitlab.com/jaxnet/hub/ead/core/blockchain/shards"
)

type ShardConnector struct {
	ShardID shards.ID
	txMan   *txutils.TxMan
}

func newShardConnector(shardConf *settings.ShardConfig) (connector *ShardConnector, err error) {
	txManager, err := keychain.KeyChain.NewTxMan(shardConf)
	if err != nil {
		return
	}

	connector = &ShardConnector{
		ShardID: shardConf.ID,
		txMan:   txManager,
	}
	return
}

var shardsConnectors map[shards.ID]*ShardConnector
var shardProcessors map[shards.ID]*ShardProcessor

func NewShardsHandler() (err error) {
	logger.Log.Info().Msg("Initializing shards handler")
	defer logger.Log.Info().Msg("Shards handler initialization finished")

	shardProcessors = make(map[shards.ID]*ShardProcessor)
	shardsConnectors = make(map[shards.ID]*ShardConnector)

	for shardID, shardConf := range settings.Conf.Blockchain.ShardsMap {
		shardConnector, err := newShardConnector(shardConf)
		if err != nil {
			return err
		}
		shardsConnectors[shardID] = shardConnector

		shardProcessor, err := newShardProcessor(shardConf)
		if err != nil {
			return err
		}
		shardProcessors[shardID] = shardProcessor
	}
	return
}

func RunShardsProcessing() {
	for _, sharProcessor := range shardProcessors {
		go sharProcessor.RunShardProcessing()
	}
}

func GetTxManForShard(shardID shards.ID) (txMan *txutils.TxMan, err error) {
	connector, ok := shardsConnectors[shardID]
	if !ok {
		err = errors.New("There are no connector for requested shard")
		return
	}
	txMan = connector.txMan
	return
}
