package processing

import (
	"ead-payments-handler/core/handlers/payments"
	"ead-payments-handler/core/handlers/transactions"
	"ead-payments-handler/core/keychain"
	"ead-payments-handler/core/logger"
	"ead-payments-handler/core/settings"
	"errors"
	"gitlab.com/jaxnet/core/indexer/clients/utxos/biggest_first"
	"gitlab.com/jaxnet/core/shard.core/cmd/tx-gatling/txutils"
	"gitlab.com/jaxnet/core/shard.core/types/chaincfg"
	"gitlab.com/jaxnet/hub/ead/core/blockchain/shards"
	"time"
)

type ShardProcessor struct {
	shardID                      shards.ID
	txMan                        *txutils.TxMan
	nonSpentUtxosClientIndexer   *biggest_first.UTXOProvider
	previousProcessedBlockNumber uint64
}

func newShardProcessor(shardConf *settings.ShardConfig) (shardProcessor *ShardProcessor, err error) {

	txManager, err := keychain.KeyChain.NewTxMan(shardConf)
	if err != nil {
		return
	}

	_, currentBlockNumber, err := txManager.RPC().GetBestBlock()
	if err != nil {
		logger.Log.Err(err).Uint64("shardID", uint64(shardConf.ID)).Msg(
			"ShP init: can't get block number from blockchain")
		return
	}

	clientIndexer, err := biggest_first.New(shardConf.NonSpentUtxosIndexerHostAndPort,
		false, keychain.KeyChain.SignDataForIndexer)
	if err != nil {
		return
	}

	shardProcessor = &ShardProcessor{
		shardID:                      shardConf.ID,
		txMan:                        txManager,
		nonSpentUtxosClientIndexer:   clientIndexer,
		previousProcessedBlockNumber: uint64(currentBlockNumber),
	}
	logger.Log.Info().Uint64("shardID", uint64(shardProcessor.shardID)).Msg("ShP initialized")
	return
}

func (shardProcessor *ShardProcessor) RunShardProcessing() {
	time.Sleep(time.Second * 5)

	for {
		logger.Log.Debug().Uint64("shardID", uint64(shardProcessor.shardID)).Msg(
			"ShP: Next iteration")

		_, currentBlockNumber, err := shardProcessor.txMan.RPC().GetBestBlock()
		if err != nil {
			logger.Log.Err(err).Uint64("shardID", uint64(shardProcessor.shardID)).Msg(
				"ShP: Can't get block number from blockchain")
			postponeShardProcessing()
			continue
		}
		logger.Log.Debug().Uint64("shardID", uint64(shardProcessor.shardID)).Int32(
			"blockNumber", currentBlockNumber).Msg("ShP: ")

		if uint64(currentBlockNumber) <= shardProcessor.previousProcessedBlockNumber {
			logger.Log.Debug().Uint64("shardID", uint64(shardProcessor.shardID)).Msg("ShP: The same block")
			postponeShardProcessing()
			continue
		}

		unprocessedPayments, err := payments.FetchPaymentsForShardWithState(
			shardProcessor.shardID, payments.Initialised)
		if err != nil {
			logger.Log.Err(err).Uint64("shardID", uint64(shardProcessor.shardID)).Msg(
				"ShP: Can't get payments from DB")
			return
		}

		filteredPayments := shardProcessor.filterPaymentsByBlockNumber(
			unprocessedPayments, uint64(currentBlockNumber))

		shardProcessor.previousProcessedBlockNumber = uint64(currentBlockNumber)

		utxoAmount, utxoRequiredCnt := shardProcessor.getPaymentsForUTXORebalancingIfNeed(
			keychain.KeyChain.AddressPubKeyHash().EncodeAddress())
		logger.Log.Debug().Uint64("shardID", uint64(shardProcessor.shardID)).Int64("utxoAmount", utxoAmount).
			Uint32("utxoRequiredCnt", utxoRequiredCnt).Msg("ShP: Rebalancing")

		if len(filteredPayments) == 0 && utxoRequiredCnt == 0 {
			logger.Log.Debug().Uint64("shardID", uint64(shardProcessor.shardID)).Msg(
				"ShP: No suitable payments and utxo for rebalancing")
			continue
		}

		txBuilder := txutils.NewTxBuilder(chaincfg.NetName(settings.Conf.Blockchain.NetworkName)).
			SetSenders(keychain.KeyChain.AddressPubKeyHash().EncodeAddress()).
			SetShardID(uint32(shardProcessor.shardID)).
			SetUTXOProvider(shardProcessor.nonSpentUtxosClientIndexer)

		if len(filteredPayments) > 0 {
			idx := uint64(0)
			for _, payment := range filteredPayments {
				logger.Log.Info().Uint64("shardID", uint64(shardProcessor.shardID)).Uint64(
					"paymentID", uint64(payment.ID)).Uint64("tx-out-num", idx).Msg("ShP: process payment")
				txBuilder.SetDestination(payment.Address, int64(payment.Amount))
				err = payments.UpdatePaymentState(payment, payments.Processing)
				if err != nil {
					logger.Log.Err(err).Uint64("shardID", uint64(shardProcessor.shardID)).Msg(
						"ShP: Can't update payment state")
					break
				}
				payment.TxOutNum = idx
				idx++
			}
			if err != nil {
				shardProcessor.resetPayments(filteredPayments)
				postponeShardProcessing()
				continue
			}
			logger.Log.Debug().Uint64("shardID", uint64(shardProcessor.shardID)).Msg(
				"ShP: All payments are added")
		}

		if utxoRequiredCnt > 0 {
			for jdx := 0; jdx < int(utxoRequiredCnt); jdx++ {
				logger.Log.Debug().Uint64("shardID", uint64(shardProcessor.shardID)).Msg("ShP: Add new utxo")
				txBuilder.SetDestination(keychain.KeyChain.AddressPubKeyHash().EncodeAddress(), utxoAmount)
			}
			logger.Log.Debug().Uint64("shardID", uint64(shardProcessor.shardID)).Msg("ShP: All utxo rebalancing are added")
		}
		txBuilder.SetChangeDestination(keychain.KeyChain.AddressPubKeyHash().EncodeAddress())

		fees, err := shardProcessor.txMan.RPC().GetExtendedFee()
		if err != nil {
			shardProcessor.resetPayments(filteredPayments)
			postponeShardProcessing()
			continue
		}
		txCntOuts := len(filteredPayments) + int(utxoRequiredCnt)
		txAmount := int64(utxoRequiredCnt) * utxoAmount
		for _, payment := range filteredPayments {
			txAmount += int64(payment.Amount)
		}
		var moderateFee int64
		var prevUtxosCnt int
		for {
			indexerResponse, err := shardProcessor.nonSpentUtxosClientIndexer.SelectForAmountWithoutLock(
				txAmount+moderateFee, uint32(shardProcessor.shardID), keychain.KeyChain.AddressPubKeyHash().EncodeAddress())
			if err != nil {
				break
			}

			var utxosSum int64
			for _, utxo := range indexerResponse {
				utxosSum += utxo.Value
			}

			moderateFee = txutils.EstimateFee(len(indexerResponse), txCntOuts, int64(fees.Moderate.SatoshiPerB), true)
			moderateFee = moderateFee * 300
			if utxosSum >= txAmount+moderateFee {
				logger.Log.Debug().Uint32("shard-id", uint32(shardProcessor.shardID)).Int64(
					"tx-amount", txAmount).Int64(
					"moderate-fee", moderateFee).Int64(
					"utxos-sum", utxosSum).Msg(
					"ShP: Enough funds for transaction + fee")
				break
			} else if prevUtxosCnt == len(indexerResponse) {
				logger.Log.Debug().Uint32("shard-id", uint32(shardProcessor.shardID)).Int64(
					"tx-amount", txAmount).Int64(
					"moderate-fee", moderateFee).Int64(
					"utxos-sum", utxosSum).Msg(
					"ShP: Not enough funds for transaction + fee")
				err = errors.New("there are no enough funds for moderate fee")
				break
			} else {
				prevUtxosCnt = len(indexerResponse)
			}
		}
		if err != nil {
			shardProcessor.resetPayments(filteredPayments)
			postponeShardProcessing()
			continue
		}

		tx, err := keychain.KeyChain.CreateTransaction(txBuilder, func(intShardID uint32) (int64, int64, error) {
			return 0, moderateFee, nil
		})
		if err != nil {
			logger.Log.Err(err).Uint64("shardID", uint64(shardProcessor.shardID)).Msg("Can't create transaction")
			shardProcessor.resetPayments(filteredPayments)
			postponeShardProcessing()
			continue
		}
		logger.Log.Info().Uint64("shardID", uint64(shardProcessor.shardID)).Str(
			"tx hash", tx.TxHash().String()).Msg("ShP: Transaction created")

		newTx := transactions.Transaction{
			ShardID:       shardProcessor.shardID,
			TxData:        tx,
			State:         transactions.Initialised,
			IsRebalancing: utxoRequiredCnt > 0,
		}

		err = transactions.WriteTransactionToDatabase(&newTx)
		if err != nil {
			logger.Log.Err(err).Uint64("shardID", uint64(shardProcessor.shardID)).Str(
				"tx hash", tx.TxHash().String()).Msg("ShP: Can't write transaction to DB")
			shardProcessor.resetPayments(filteredPayments)
			postponeShardProcessing()
			continue
		}

		for _, payment := range filteredPayments {
			err = payments.UpdatePaymentStateAndTxID(payment, payments.Pending, newTx.ID)
			if err != nil {
				logger.Log.Err(err).Uint64("shardID", uint64(shardProcessor.shardID)).Str(
					"tx hash", tx.TxHash().String()).Uint64("payment id", payment.ID).Msg(
					"ShP: Can't update payment state and txID in DB")
				postponeShardProcessing()
				continue
			}
		}

		_, err = shardProcessor.txMan.RPC().SendRawTransaction(tx)
		if err != nil {
			logger.Log.Err(err).Uint64("shardID", uint64(shardProcessor.shardID)).Str(
				"tx hash", tx.TxHash().String()).Msg("ShP: Can't send transaction to blockchain")
			// todo : update transaction state
			shardProcessor.resetPayments(filteredPayments)
			postponeShardProcessing()
			continue
		}
		logger.Log.Info().Uint64("shardID", uint64(shardProcessor.shardID)).Str(
			"tx hash", tx.TxHash().String()).Msg("Transaction sent")

		err = transactions.UpdateTransactionState(&newTx, transactions.Pending)
		if err != nil {
			return
		}

		postponeShardProcessing()
	}
}

func (shardProcessor *ShardProcessor) filterPaymentsByBlockNumber(
	paymentsList map[uint64]*payments.Payment, currentBlockNumber uint64) (
	paymentsListFiltered map[uint64]*payments.Payment) {

	paymentsListFiltered = make(map[uint64]*payments.Payment)
	var currentPaymentsInTransaction uint32
	currentPaymentsInTransaction = 0
	for id, payment := range paymentsList {
		if payment.MaxBlocksHeight < currentBlockNumber {
			logger.Log.Info().Uint64("shardID", uint64(shardProcessor.shardID)).Uint64(
				"payment id", payment.ID).Uint64("payment max block height", payment.MaxBlocksHeight).Msg(
				"ShP: Payment invalid by max block height")
			err := payments.UpdatePaymentState(payment, payments.Expired)
			if err != nil {
				logger.Log.Err(err).Uint64("shardID", uint64(shardProcessor.shardID)).Uint64(
					"payment id", payment.ID).Msg("ShP: Can't update payment state")
			}
			continue
		}
		paymentsListFiltered[id] = payment
		currentPaymentsInTransaction++
		if currentPaymentsInTransaction >= settings.Conf.Handler.Payments.TransactionsPerBlock {
			logger.Log.Info().Uint64("shardID", uint64(shardProcessor.shardID)).Msg(
				"ShP: Max payments per transaction")
			return
		}
	}
	return
}

func postponeShardProcessing() {
	time.Sleep(time.Second * time.Duration(settings.Conf.Handler.Blockchain.Shards.AvgBlockGenerationTimeSeconds))
}

func (shardProcessor *ShardProcessor) resetPayments(paymentsList map[uint64]*payments.Payment) {
	for _, payment := range paymentsList {
		err := payments.ResetPayment(payment)
		if err != nil {
			logger.Log.Err(err).Uint64(
				"shardID", uint64(shardProcessor.shardID)).Uint64(
				"payment id", payment.ID).Msg(
				"ShP: Can't reset payment state in DB")
		}
	}
}

func (shardProcessor *ShardProcessor) getPaymentsForUTXORebalancingIfNeed(eadAddress string) (
	amount int64, count uint32) {

	isRebalansingTxPresent, err := transactions.IsRebalansingTransactionsWithStatePresent(
		shardProcessor.shardID, transactions.Pending)
	if err != nil {
		return
	}

	if isRebalansingTxPresent {
		logger.Log.Debug().Msg("Payment rebalancing transaction in pending status")
		return
	}

	total, err := shardProcessor.nonSpentUtxosClientIndexer.NonSpentUTXOsCnt(eadAddress)
	if err != nil {
		return
	}

	logger.Log.Debug().Uint64("shardID", uint64(shardProcessor.shardID)).
		Uint64("total", total).Msg("ShP: getPaymentsForUTXORebalansingIfNeed")
	if total >= uint64(settings.Conf.Handler.MinRequiredFeesUTXOCnt) {
		return
	}
	fee, err := shardProcessor.getCurrentHighFee()
	if err != nil {
		return
	}
	// get double fee value to ensure that amount will be enough for network fee
	amount = fee * 2
	logger.Log.Debug().Uint64("shardID", uint64(shardProcessor.shardID)).
		Int64("utxo amount", amount).Msg("ShP: getPaymentsForUTXORebalansingIfNeed")
	count = settings.Conf.Handler.RebalancingUTXOsCnt
	return
}

func (shardProcessor *ShardProcessor) getCurrentHighFee() (fee int64, err error) {
	extendedFeeResult, err := shardProcessor.txMan.RPC().GetExtendedFee()
	if err != nil {
		return
	}
	// cross shard tx contains 4 inputs and 4 outputs
	fee = txutils.EstimateFee(4, 4, int64(extendedFeeResult.Fast.SatoshiPerB), true)
	return
}
