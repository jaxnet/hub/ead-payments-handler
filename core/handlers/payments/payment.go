package payments

import (
	"errors"
	"fmt"
	"github.com/google/uuid"
	"gitlab.com/jaxnet/core/shard.core/btcutil"
	"gitlab.com/jaxnet/hub/ead/core/blockchain/shards"
	"gitlab.com/jaxnet/hub/ead/core/ec"
	"gitlab.com/jaxnet/hub/ead/core/settings"
	"time"
)

var (
	ErrInvalidTxState = errors.New("invalid tx state")
	ErrInvalidTxData  = errors.New("invalid tx data")
)

const (
	Initialised = 0
	Processing  = 1
	Pending     = 2
	Confirmed   = 3
	Expired     = 4
	Error       = 255
)

type Payment struct {
	ID              uint64
	UUID            uuid.UUID
	State           uint8
	Amount          btcutil.Amount
	Address         string
	ShardID         shards.ID
	MaxBlocksHeight uint64
	TransactionID   uint64
	TxOutNum        uint64
	Created         *time.Time // Could be nil.
}

func FromDBData(id int64, binaryUUID [16]byte, shardID shards.ID, amount btcutil.Amount, address string,
	maxBlocksHeight uint64, state int16, txID int64, txOutNum uint64) (payment *Payment, err error) {

	payment = &Payment{}
	payment.ID, err = ParseAndValidateID(id)
	if err != nil {
		return
	}

	payment.UUID, err = ParseAndValidateUUID(binaryUUID)
	if err != nil {
		return
	}

	payment.Amount, err = ParseAndValidateAmount(amount)
	if err != nil {
		return
	}

	payment.ShardID = shardID

	payment.Address = address

	payment.MaxBlocksHeight = maxBlocksHeight

	payment.State, err = ParseAndValidateState(state)
	if err != nil {
		return
	}

	payment.TransactionID, err = ParseAndValidateID(txID)
	if err != nil {
		return
	}

	payment.TxOutNum = txOutNum

	return
}

func ParseAndValidateID(data int64) (id uint64, err error) {
	if data < 0 {
		err = ErrInvalidTxData
	}

	id = uint64(data)
	return
}

func ParseAndValidateUUID(binary [16]byte) (uid uuid.UUID, err error) {
	err = uid.UnmarshalBinary(binary[:])
	if err != nil {
		err = ErrInvalidTxData
		return
	}

	return
}

func ParseAndValidateAmount(amount btcutil.Amount) (a btcutil.Amount, err error) {
	if amount < 0 {
		err = ErrInvalidTxData
		return
	}

	a = amount
	return
}

func ParseBlockchainAddress(a string) (address btcutil.Address, err error) {
	address, err = btcutil.DecodeAddress(a, settings.NetParams)
	if err != nil {
		err = ErrInvalidTxData
		return
	}

	return
}

func ParseMultiSigBlockchainAddress(a *string) (address btcutil.AddressScriptHash, err error) {
	if a == nil {
		return // empty address
	}

	addr, err := btcutil.DecodeAddress(*a, settings.NetParams)
	if err != nil {
		err = ErrInvalidTxData
		return
	}

	tmp, ok := addr.(*btcutil.AddressScriptHash)
	if !ok {
		err = ErrInvalidTxData
		return
	}

	address = *tmp
	return
}

func ParseAndValidateState(state int16) (stateCode uint8, err error) {
	stateCode = uint8(state)

	//for availableState, _ := range TxStatesGraph {
	//	if stateCode == availableState {
	//		return
	//	}
	//}
	//
	//err = fmt.Errorf(
	//	"unexpected tx state occured. Valid states are: %v (%v, %v)",
	//	TxStatesGraph, ErrInvalidTxData, ErrInvalidTxState)
	return
}

func ParseAndValidateTime(t *time.Time, nullIsOK bool) (updated *time.Time, err error) {
	if !nullIsOK && t == nil {
		err = fmt.Errorf("time is required to be not null: %v", ec.ErrNilArgument)
		return
	}

	// todo: add validation (if any)
	updated = t
	return
}
