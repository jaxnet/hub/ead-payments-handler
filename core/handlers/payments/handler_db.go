package payments

import (
	"context"
	"ead-payments-handler/core/database"
	"errors"
	"fmt"
	"github.com/google/uuid"
	"gitlab.com/jaxnet/core/shard.core/btcutil"
	"gitlab.com/jaxnet/hub/ead/core/blockchain/shards"
)

func WritePaymentToDatabase(payment Payment) (err error) {
	tx, err := database.DB.Begin(context.Background())
	if err != nil {
		return
	}

	// Rollback is safe to call even if the tx is already closed, so if
	// the tx commits successfully, this is a no-op
	defer tx.Rollback(context.Background())

	row := tx.QueryRow(context.Background(),
		"INSERT INTO payments (uuid, shard_id, amount, address, max_blocks_height, state) "+
			"VALUES ($1, $2, $3, $4, $5, $6) RETURNING id",
		payment.UUID.String(),
		payment.ShardID,
		payment.Amount,
		payment.Address,
		payment.MaxBlocksHeight,
		Initialised)

	var recordID uint64
	err = row.Scan(&recordID)
	if err != nil {
		err = fmt.Errorf("can't write new payments to the database: %w", err)
		return
	}

	err = tx.Commit(context.Background())
	return
}

func FetchPaymentsForShardWithState(
	shardID shards.ID, state uint8) (paymentsList map[uint64]*Payment, err error) {
	query := fmt.Sprintf(
		"SELECT "+
			"id, uuid, shard_id, amount, address, max_blocks_height, state, txid, tx_out_num "+
			"FROM payments "+
			"WHERE shard_id = %v and state = %v ORDER BY created", shardID, state)

	rows, err := database.DB.Query(context.Background(), query)
	if err != nil {
		return
	}
	defer rows.Close()

	paymentsList = make(map[uint64]*Payment)
	for rows.Next() {
		values, err := rows.Values()
		if err != nil {
			return nil, err
		}

		var txID int64
		switch v := values[7].(type) {
		case int64:
			txID = v
		}
		var txOutNum uint64
		switch v := values[8].(type) {
		case uint64:
			txOutNum = v
		}

		payment, err := FromDBData(
			values[0].(int64),
			values[1].([16]byte),
			shards.ID(values[2].(int64)),
			btcutil.Amount(values[3].(int64)),
			values[4].(string),
			uint64(values[5].(int64)),
			values[6].(int16),
			txID,
			txOutNum)

		if err != nil {
			return nil, err
		}

		paymentsList[payment.ID] = payment
	}
	return
}

func UpdatePaymentState(payment *Payment, newState uint8) (err error) {
	tx, err := database.DB.Begin(context.Background())
	if err != nil {
		return
	}

	// Rollback is safe to call even if the tx is already closed, so if
	// the tx commits successfully, this is a no-op
	defer tx.Rollback(context.Background())

	_, err = tx.Exec(context.Background(),
		"UPDATE payments SET state = $1 where id = $2",
		newState,
		payment.ID)
	if err != nil {
		return
	}

	err = tx.Commit(context.Background())
	if err != nil {
	}
	payment.State = newState
	return
}

func UpdatePaymentStateAndTxID(payment *Payment, newState uint8, newTxID uint64) (err error) {
	tx, err := database.DB.Begin(context.Background())
	if err != nil {
		return
	}

	// Rollback is safe to call even if the tx is already closed, so if
	// the tx commits successfully, this is a no-op
	defer tx.Rollback(context.Background())

	_, err = tx.Exec(context.Background(),
		"UPDATE payments SET state = $1, txid = $2, tx_out_num = $3 where id = $4",
		newState,
		newTxID,
		payment.TxOutNum,
		payment.ID)
	if err != nil {
		return
	}

	err = tx.Commit(context.Background())
	payment.State = newState
	return
}

func UpdatePaymentsStateByTransactionID(transactionID uint64, newState uint8) (err error) {
	tx, err := database.DB.Begin(context.Background())
	if err != nil {
		return
	}

	// Rollback is safe to call even if the tx is already closed, so if
	// the tx commits successfully, this is a no-op
	defer tx.Rollback(context.Background())

	_, err = tx.Exec(context.Background(),
		"UPDATE payments SET state = $1 where txid = $2",
		newState,
		transactionID)
	if err != nil {
		return
	}

	err = tx.Commit(context.Background())
	return
}

func ResetPayment(payment *Payment) (err error) {
	tx, err := database.DB.Begin(context.Background())
	if err != nil {
		return
	}

	// Rollback is safe to call even if the tx is already closed, so if
	// the tx commits successfully, this is a no-op
	defer tx.Rollback(context.Background())

	_, err = tx.Exec(context.Background(),
		"UPDATE payments SET state = $1, txid = null where id = $2",
		Initialised,
		payment.ID)
	if err != nil {
		return
	}

	err = tx.Commit(context.Background())
	payment.State = Initialised
	return
}

func CheckIfPaymentExists(uuid uuid.UUID) (exists bool, err error) {
	rows, err := database.DB.Query(context.Background(),
		"SELECT 1 FROM payments WHERE uuid = $1",
		uuid.String())
	if err != nil {
		return
	}
	defer rows.Close()

	exists = rows.Next()
	return
}

func FetchPaymentByUUID(uuid uuid.UUID) (payment *Payment, err error) {
	rows, err := database.DB.Query(context.Background(),
		"SELECT id, uuid, shard_id, amount, address, max_blocks_height, state, txid, tx_out_num "+
			"FROM payments WHERE uuid = $1",
		uuid.String())
	if err != nil {
		return
	}
	defer rows.Close()

	if rows.Next() {
		values, err := rows.Values()
		if err != nil {
			return nil, err
		}

		var txID int64
		switch v := values[7].(type) {
		case int64:
			txID = v
		}
		var txOutNum uint64
		switch v := values[8].(type) {
		case int64:
			txOutNum = uint64(v)
		}

		payment, err = FromDBData(
			values[0].(int64),
			values[1].([16]byte),
			shards.ID(values[2].(int64)),
			btcutil.Amount(values[3].(int64)),
			values[4].(string),
			uint64(values[5].(int64)),
			values[6].(int16),
			txID,
			txOutNum)

		if err != nil {
			return nil, err
		}

		// todo : check if rows.Next()
	} else {
		err = errors.New("There are no requested payment")
	}
	return
}

func FetchPaymentsForTransaction(txID uint64) (paymentsList map[uint64]*Payment, err error) {
	query := fmt.Sprintf(
		"SELECT "+
			"id, uuid, shard_id, amount, address, max_blocks_height, state, tx_out_num "+
			"FROM payments "+
			"WHERE txid = %v ", txID)

	rows, err := database.DB.Query(context.Background(), query)
	if err != nil {
		return
	}
	defer rows.Close()

	paymentsList = make(map[uint64]*Payment)
	for rows.Next() {
		values, err := rows.Values()
		if err != nil {
			return nil, err
		}

		payment, err := FromDBData(
			values[0].(int64),
			values[1].([16]byte),
			shards.ID(values[2].(int64)),
			btcutil.Amount(values[3].(int64)),
			values[4].(string),
			uint64(values[5].(int64)),
			values[6].(int16),
			int64(txID),
			values[7].(uint64))

		if err != nil {
			return nil, err
		}

		paymentsList[payment.ID] = payment
	}
	return
}
