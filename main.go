package main

import (
	"ead-payments-handler/core"
	"ead-payments-handler/core/logger"
	"ead-payments-handler/core/settings"
	"gitlab.com/jaxnet/hub/ead/core/ec"
)

func main() {
	err := settings.LoadSettings()
	ec.InterruptOnError(err)

	logger.Init()

	err = core.NewCore()
	ec.InterruptOnError(err)

	err = core.RunUntilError()
	ec.InterruptOnError(err)
}
