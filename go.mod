module ead-payments-handler

go 1.14

require (
	github.com/dgraph-io/ristretto v0.1.0 // indirect
	github.com/gocarina/gocsv v0.0.0-20210408192840-02d7211d929d // indirect
	github.com/golang/glog v0.0.0-20210429001901-424d2337a529 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/google/uuid v1.2.0
	github.com/jackc/pgproto3/v2 v2.0.7 // indirect
	github.com/jackc/pgx/v4 v4.11.0
	github.com/jessevdk/go-flags v1.5.0 // indirect
	github.com/klauspost/cpuid/v2 v2.0.6 // indirect
	github.com/minio/sha256-simd v1.0.0 // indirect
	github.com/patrickmn/go-cache v2.1.0+incompatible // indirect
	github.com/prometheus/common v0.28.0 // indirect
	github.com/prometheus/procfs v0.6.0 // indirect
	github.com/rs/zerolog v1.22.0
	github.com/vmihailenco/msgpack/v5 v5.3.1 // indirect
	gitlab.com/jaxnet/core/indexer v0.0.0-20210518041130-22dce9d416f9
	gitlab.com/jaxnet/core/shard.core v1.6.2-0.20210507145058-b27feef6ab2d
	gitlab.com/jaxnet/hub/ead v0.0.0-20210507131105-ec277fd09aa5
	golang.org/x/text v0.3.6 // indirect
	gopkg.in/natefinch/lumberjack.v2 v2.0.0
	gopkg.in/yaml.v2 v2.4.0
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)

replace gitlab.com/jaxnet/core/indexer => ./dep/indexer
